PDFLATEX = pdflatex
TARGETS  = paper.pdf
SOURCE   = paper.tex
BIBSRC   = petsc.bib petscapp.bib
TEX_SUFS = .aux .log .nav .out .snm .toc .tdo .vrb
DEPS     = ${BIBSRC}

all: ${TARGETS}

petsc.bib:
	ln -s ${PETSC_DIR}/src/docs/tex/$@ $@
petscapp.bib:
	ln -s ${PETSC_DIR}/src/docs/tex/$@ $@

paper.pdf: paper.tex ${DEPS}
	${PDFLATEX} $<
	BSTINPUTS=${BSTINPUTS}:./styles BIBINPUTS=${BIBINPUTS}:${PETSC_DIR}/src/docs/tex:${HOME}/Documents bibtex $(subst .tex, , ${<})
	${PDFLATEX} $<
	${PDFLATEX} $<

single: ${SOURCE}
	${PDFLATEX} $<

clean:
	rm $(foreach suf, ${TEX_SUFS}, *${suf}) || true

cleaner: clean
	rm ${TARGETS}

